# Recipe App API Proxy

NGINX Proxy app for our recipe app API

## Usage 

### Environment Variables

   * `LISTEN_PORT` = Port to listen on (default: `8000`)
   * `APP_HOST` = Hostname of app to forward requests to (default: `app`)
   * `APP_PORT` = Post of the app to forward the requests to (default: `9000`)